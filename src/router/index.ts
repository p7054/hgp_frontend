import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import NotFoundView from "../views/NotFoundView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/privacy",
      name: "privacy",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/PrivacyPolicyView.vue"),
    },
    {
      path: "/signUp",
      name: "signUp",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/SignUpView.vue"),
    },
    {
      path: "/products/:id",
      name: "product",
      props: true,
      component: () => import("../views/ProductView.vue"),
    },
    {
      path: "/users/:id",
      name: "user",
      props: true,
      component: () => import("../views/UserView.vue"),
    },
    {
      path: "/add",
      name: "productAdd",
      props: true,
      component: () => import("../views/AddProductView.vue"),
    },
    {
      path: "/404",
      name: "NotFound",
      component: NotFoundView,
    },
    {
      path: "/:pathMatch(.*)*",
      redirect: "/404",
    },
  ],
  scrollBehavior() {
    return {
      top: 0,
    };
  },
});

export default router;
