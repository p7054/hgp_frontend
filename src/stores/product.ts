import router from "@/router";
import { defineStore } from "pinia";

// Use this when the bug is fixed
export type ProductDetails = {
  id?: string;
  name: string;
  price: string | number;
  description: string;
  condition: string;
  region?: string;
  date?: string;
  solved?: boolean;
  id_user?: string;
  address: {
    city: string;
    region: string;
    street: string;
    building: string;
    flat: string;
  };
};

export const useProductStore = defineStore("product", {
  state: () => {
    return {
      products: [] as ProductDetails[],
      userProducts: [] as ProductDetails[],

      page: 0,
      pageLimit: 24,
    };
  },
  actions: {
    async getProducts(page: number, pageLimit: number) {
      const fetchUrl = () => {
        return (
          "https://retoolapi.dev/9VNkeo/post?solved=false&_page=" +
          page +
          "&_limit=" +
          pageLimit
        );
      };
      const response = await fetch(fetchUrl())
        .then((res) => res.json())
        .then((data) => (this.products = data));
      return response;
    },
    async getProductById(id: string | undefined) {
      const fetchUrl = () => {
        return "https://retoolapi.dev/9VNkeo/post/" + id;
      };
      const response = await fetch(fetchUrl())
        .then((res) => {
          if (res.ok) {
            return res.json();
          }
          router.push("/404");
        })
        .then((data) => this.products.push(data));
      return response;
    },
    async getUserProducts(userId: string | undefined) {
      const fetchUrl = () => {
        return "https://retoolapi.dev/9VNkeo/post?id_user=" + userId;
      };
      const response = await fetch(fetchUrl())
        .then((res) => res.json())
        .then((data) => (this.userProducts = data));
      return response;
    },
    async addProduct(userId: string, post: ProductDetails) {
      const rawResponse = await fetch("https://retoolapi.dev/9VNkeo/post", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: Date.now(),
          name: post.name,
          price: post.price,
          description: post.description,
          date: Math.floor(new Date().getTime() / 1000),
          solved: false,
          id_user: userId,
        }),
      });
      const content = await rawResponse.json();
      console.log("content is", content);
    },
  },
  getters: {
    productInfo: (state) => state.products,
    userProductInfo: (state) => state.userProducts,
  },
});
