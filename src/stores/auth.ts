import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", {
  state: () => {
    return {
      loggedIn: false,
    };
  },
  actions: {
    login() {
      const loginUrl = "http://localhost:8080/users/login";
      // fetch()
      const d = new Date();
      d.setTime(d.getTime() + 1 * 24 * 60 * 60 * 1000);
      const expires = "expires=" + d.toUTCString();
      document.cookie =
        "Token=" + "response.data.Token" + ";" + expires + ";path=/";
    },

    async register() {
      const registerUrl = "http://83.246.199.135:8080/auth/register";

      const d = new Date();
      d.setTime(d.getTime() + 1 * 24 * 60 * 60 * 1000);
      const expires = "expires=" + d.toUTCString();
      const response = await fetch(registerUrl, {
        method: "POST",
        headers: {
          credentials: "include",
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: new Date(),
          name: "vasya",
          password: "123123",
        }),
      });

      // document.cookie =
      //   "Token=" +
      //   response.headers.get("Set-Cookie").Token +
      //   ";" +
      //   expires +
      //   ";path=/";
      console.log("Test is " + response.headers.get("Set-Cookie"));
    },
  },
  getters: {},
});
