import { defineStore } from "pinia";

export const useReviewStore = defineStore("review", {
  state: () => {
    return {
      reviews: [],

      page: 0,
      pageLimit: 15,
    };
  },
  actions: {
    async getReviews(page: number, pageLimit: number) {
      const fetchUrl = () => {
        return (
          "https://retoolapi.dev/96sHMM/rating?_page=" +
          page +
          "&_limit=" +
          pageLimit
        );
      };
      const response = await fetch(fetchUrl())
        .then((res) => res.json())
        .then((data) => (this.reviews = data));
      return response;
    },
  },
  getters: {
    reviewsInfo: (state) => state.reviews,
  },
});
