import router from "@/router";
import { defineStore } from "pinia";

export type UserDetails = {
  id: string;
  name: string;
  date: string;
  id_photo: number;
};

export const useUserStore = defineStore("user", {
  state: () => {
    return {
      users: [] as UserDetails[],
    };
  },
  actions: {
    async getUserById(id: string | undefined) {
      const fetchUrl = () => {
        return "https://retoolapi.dev/ecf8dR/users/" + id;
      };
      const response = await fetch(fetchUrl())
        .then((res) => {
          if (res.ok) {
            return res.json();
          }
          router.push("/404");
        })
        .then((data) => this.users.push(data));

      return response;
    },
  },
  getters: {
    userInfo: (state) => state.users,
  },
});
// https://retoolapi.dev/96sHMM/rating
// https://retoolapi.dev/9VNkeo/post
